function installerClickSurLiensDeGauche() {
    var liens = menulateral.getElementsByTagName("a");

    for (var i = 0; i < liens.length; i++) {
        liens[i].onclick=function(e) {
            MontrerReponse(this); return false;
        }
    }
}


function CacherReponses() {
    var reponses = content.getElementsByTagName("div");
    for (var i = 0; i < reponses.length; i++)  {
        reponses[i].style.display="none";
    }
    /* document.getElementsByTagName("dd")[i].style.display="none"  est une syntaxe possible    */
}

function MontrerReponse(lienhref) {
    var sectionToDisplay = lienhref.className,
        /* Cheating a little here because document.querySelectorAll gives us a NodeList, not an array (even though it looks like such) */
        sections = Array.prototype.slice.call(document.querySelectorAll("div.section"));

        sections.forEach(function (section) {
        var display = 'none';
        if (sectionToDisplay == section.id) {
            display = 'block';

        }

        section.style.display = display;
        presentation.style.display = 'none';
    });
}