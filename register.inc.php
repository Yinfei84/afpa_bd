<?php
// on teste si le visiteur a soumis le formulaire
if (isset($_POST['inscription']) && $_POST['inscription'] == 'Inscription') {
  // on teste l'existence de nos variables. On teste également si elles ne sont pas vides
  if ((isset($_POST['NOM_CLIENT']) && !empty($_POST['NOM_CLIENT'])) && (isset($_POST['MOT_DE_PASSE']) && !empty($_POST['MOT_DE_PASSE'])) && (isset($_POST['MOT_DE_PASSE_confirm']) && !empty($_POST['MOT_DE_PASSE_confirm']))) {
    // on teste les deux mots de passe
    if ($_POST['MOT_DE_PASSE'] != $_POST['MOT_DE_PASSE_confirm']) {
      $erreur2 = 'Les 2 mots de passe sont différents.';
    }
    else {

      // on recherche si ce NOM_CLIENT est déjà utilisé par un autre membre
      $sql_membre = 'SELECT count(*) FROM client WHERE NOM_CLIENT="'.$_POST['NOM_CLIENT'].'"';
      $req_membre = mysql_query($sql_membre) or die('Erreur SQL !<br />'.$sql_membre.'<br />'.mysql_error());
      $data = mysql_fetch_array($req_membre);
      
      if ($data[0] == 0) {
        $sql_membre = 'INSERT INTO client VALUES("", "'.$_POST['COURRIEL'].'", "'.$_POST['NOM_CLIENT'].'", "'.$_POST['PRENOM_CLIENT'].'", "'.$_POST['ADRESSE'].'", "'.$_POST['TEL'].'", "'.$_POST['MOT_DE_PASSE'].'", "'.$_POST['ADRESSE_LIVRAISON'].'", "","")';
        mysql_query($sql_membre) or die('Erreur SQL !'.$sql_membre.'<br />'.mysql_error());
        $_SESSION['NOM_CLIENT'] = $_POST['NOM_CLIENT'];
        header('Location:index.php');
      }
      else {
        $erreur2 = 'Un membre possède déjà ce Nom.';
      }
    }
  }
  else {
    $erreur2 = 'Au moins un des champs est vide.';
  }
}
?>
<form action="index.php" method="post">
  <?php if (isset($erreur2)) echo $erreur2;?>
    <center><table border=1>
    <tr><td style="text-align:right">Email</td><td><input type="text" name="COURRIEL" value="<?php if (isset($_POST['COURRIEL'])) echo htmlentities(trim($_POST['COURRIEL'])); ?>"></td></tr>
    <tr><td style="text-align:right">Nom</td><td><input type="text" name="NOM_CLIENT" value="<?php if (isset($_POST['NOM_CLIENT'])) echo htmlentities(trim($_POST['NOM_CLIENT'])); ?>"></td></tr>
    <tr><td style="text-align:right">Prénom</td><td><input type="text" name="PRENOM_CLIENT" value="<?php if (isset($_POST['PRENOM_CLIENT'])) echo htmlentities(trim($_POST['PRENOM_CLIENT'])); ?>"></td></tr>
    <tr><td style="text-align:right">Adresse</td><td><input type="text" name="ADRESSE" value="<?php if (isset($_POST['ADRESSE'])) echo htmlentities(trim($_POST['ADRESSE'])); ?>"></td></tr>
    <tr><td style="text-align:right">Téléphone</td><td><input type="text" name="TEL" value="<?php if (isset($_POST['TEL'])) echo htmlentities(trim($_POST['TEL'])); ?>"></td></tr>
    <tr><td style="text-align:right">Mot de passe</td><td><input type="password" name="MOT_DE_PASSE" value="<?php if (isset($_POST['MOT_DE_PASSE'])) echo htmlentities(trim($_POST['MOT_DE_PASSE'])); ?>"></td></tr>
    <tr><td style="text-align:right">Confirmation du mot de passe</td><td><input type="password" name="MOT_DE_PASSE_confirm" value="<?php if (isset($_POST['MOT_DE_PASSE_confirm'])) echo htmlentities(trim($_POST['MOT_DE_PASSE_confirm'])); ?>"></td></tr>
    <tr style="text-align:right"><td >Adresse de livraison</td><td><input type="text" name="ADRESSE_LIVRAISON" value="<?php if (isset($_POST['ADRESSE_LIVRAISON'])) echo htmlentities(trim($_POST['ADRESSE_LIVRAISON'])); ?>"></td></tr>
  <tr><td colspan=2><center><input type="submit" name="inscription" value="Inscription"></center></td></table></center>
</form>