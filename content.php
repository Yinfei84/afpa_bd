<div id="content">

	<!-- Formulaire d'inscription nouvel utilisteur -->
	<?php if (!isset($_SESSION['NOM_CLIENT'])) { ?>  
	<center><p style="text-align:left">Vous n'êtes pas encore membre de notre site ? <br> N'hésitez plus et rejoignez nous en remplissant le formulaire d'inscription en <a href="index.php?page=register"> [cliquant ici] !</a></p></center>
	<?php } ?>
	<?php if (!isset($_SESSION['NOM_CLIENT']) && isset($_SESSION['page']) && $_SESSION['page']=='register' ){ ?>	
	<section id="inscription">
	<?php include "register.inc.php"; ?>
	</section>
	<?php } ?>

	<?php if (isset($_SESSION['NOM_CLIENT']) && !isset($_SESSION['page']) && $_SESSION['NOM_CLIENT']!='Administrateur'){ ?>
	<div id="presentation">
		<p>Bienvenue sur notre site <?php echo $_SESSION['NOM_CLIENT']; ?>.</p>
		<p>Utilisez les liens sur votre gauche pour accéder aux modules de recherche et à votre espace personnel.</p>
		<p>L'équipe du site vous souhaite une agréable visite !</p>
	</div>
	<?php } ?>

	<!-- Espace personnel -->
	<?php if (isset($_SESSION['NOM_CLIENT']) && $_SESSION['NOM_CLIENT']!='Administrateur' && isset($_SESSION['page']) && $_SESSION['page']=='espace_perso' ){ ?>
	<div id="espace_perso">
	<center><h3>Changer mes infos personnelles</h3></center>
	<?php include 'updateinfo.inc.php' ?>
	<center><h3>Mes paniers</h3></center>
	<?php include 'paniersperso.inc.php' ?>
	</div>
	<?php } ?>

	<!-- Module de recherche standard -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechsimple'){ ?>
	<?php include "rechstandard.inc.php" ; ?>
	<?php } ?>

	<!-- Recherche des oeuvres par auteurs -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechauteur'){ ?>
	<div id="rechauteur">
	<center><h3>Recherche par auteurs</h3></center>
	<?php include "rechauteur.inc.php" ; ?>
	</div>
	<?php } ?>
		
	<!-- Recherche des oeuvres par titres -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechtitre'){ ?>
	<div id="rechtitre">
	<center><h3>Recherche par titres</h3></center>
	<?php include "rechtitre.inc.php" ;?>
	</div>
	<?php } ?>
		
	<!-- Recherche des oeuvres par genre -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechgenre'){ ?>
	<div id="rechgenre">
	<center><h3>Recherche par genre</h3></center>
	<?php include "rechgenre.inc.php" ; ?>
	</div>
	<?php } ?>

	<!-- Affichage du panier -->
	<div id="panier">
	<?php include "detail_panier.php"; ?>
	</div>
		
	<!-- Recherche des oeuvres par éditeurs -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechediteur'){ ?>
	<div id="rechediteur">
	<center><h3>Recherche par editeur</h3></center>
	<?php include "rechediteur.inc.php" ; ?>
	</div>
	<?php } ?>

	<!-- Recherche Multicritère -->
	<?php if(isset($_SESSION['page']) && $_SESSION['page']=='rechmulti'){ ?>
	<center><h3>Recherche Multi-criteres</h3></center>
	<?php include "rechmulticritere.inc.php" ; ?>
	<?php } ?>

	<?php if ((isset($_SESSION['NOM_CLIENT']) && $_SESSION['NOM_CLIENT']=='Administrateur')){ ?>
	<section id="outils_admin">
		
		<!-- ajout de produit dans la base de données -->
		<center><h3>Ajout de produit</h3></center>
		<?php include "ajoutproduit.inc.php" ; ?>
		
		<!-- modification de produit dans la base de données -->
		<center><h3>Modifier le produit</h3></center>
		<p>Sélectionner le produit via les modules de recherche pour les modifier.</p>
		<?php include "modifproduit.inc.php" ; ?>
		
		<!-- modification de commandes dans la base de données -->
		<center><h3>Modifier une commande</h3></center>
		<?php include "modifcommande.inc.php" ; ?>
	</section>
	<?php } ?>
</div>