<?php

/**
 * Verifie si le panier existe, le créé sinon
 * @return booleen
 */
function creationPanier(){
   if (!isset($_SESSION['panier'])){
      $_SESSION['panier']=array();
      $_SESSION['panier']['libelleProduit'] = array();
      $_SESSION['panier']['qteProduit'] = array();
      $_SESSION['panier']['prixProduit'] = array();
      $_SESSION['panier']['verrou'] = false;
   }
   return true;
}


/**
 * Ajoute un article dans le panier
 * @param string $libelleProduit
 * @param int $qteProduit
 * @param float $prixProduit
 * @return void
 */
function ajouterArticle($libelleProduit,$qteProduit,$prixProduit){

   //Si le panier existe
   if (creationPanier() && !isVerrouille())
   {
      //Si le produit existe déjà on ajoute seulement la quantité
      $positionProduit = array_search($libelleProduit,  $_SESSION['panier']['libelleProduit']);

      if ($positionProduit !== false)
      {
         $_SESSION['panier']['qteProduit'][$positionProduit] += $qteProduit ;
      }
      else
      {
         //Sinon on ajoute le produit
         array_push( $_SESSION['panier']['libelleProduit'],$libelleProduit);
         array_push( $_SESSION['panier']['qteProduit'],$qteProduit);
         array_push( $_SESSION['panier']['prixProduit'],$prixProduit);
      }
   }
   else
   echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}



/**
 * Modifie la quantité d'un article
 * @param $libelleProduit
 * @param $qteProduit
 * @return void
 */
function modifierQTeArticle($libelleProduit,$qteProduit){
   //Si le panier éxiste
   if (creationPanier() && !isVerrouille())
   {
      //Si la quantité est positive on modifie sinon on supprime l'article
      if ($qteProduit > 0)
      {
         //Recharche du produit dans le panier
         $positionProduit = array_search($libelleProduit,  $_SESSION['panier']['libelleProduit']);

         if ($positionProduit !== false)
         {
            $_SESSION['panier']['qteProduit'][$positionProduit] = $qteProduit ;
         }
      }
      else
      supprimerArticle($libelleProduit);
   }
   else
   echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}

/**
 * Supprime un article du panier
 * @param $libelleProduit
 * @return unknown_type
 */
function supprimerArticle($libelleProduit){
   //Si le panier existe
   if (creationPanier() && !isVerrouille())
   {
      //Nous allons passer par un panier temporaire
      $tmp=array();
      $tmp['libelleProduit'] = array();
      $tmp['qteProduit'] = array();
      $tmp['prixProduit'] = array();
      $tmp['verrou'] = $_SESSION['panier']['verrou'];

      for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
      {
         if ($_SESSION['panier']['libelleProduit'][$i] !== $libelleProduit)
         {
            array_push( $tmp['libelleProduit'],$_SESSION['panier']['libelleProduit'][$i]);
            array_push( $tmp['qteProduit'],$_SESSION['panier']['qteProduit'][$i]);
            array_push( $tmp['prixProduit'],$_SESSION['panier']['prixProduit'][$i]);
         }

      }
      //On remplace le panier en session par notre panier temporaire à jour
      $_SESSION['panier'] =  $tmp;
      //On efface notre panier temporaire
      unset($tmp);
   }
   else
   echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}


/**
 * Montant total du panier
 * @return int
 */
function MontantGlobal(){
   $total=0;
   for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
   {
      $total += $_SESSION['panier']['qteProduit'][$i] * $_SESSION['panier']['prixProduit'][$i];
   }
   return $total;
}


/**
 * Fonction de suppression du panier
 * @return void
 */
function supprimePanier(){
   unset($_SESSION['panier']);
   header('Location: index.php');
}

/**
 * Permet de savoir si le panier est verrouillé
 * @return booleen
 */
function isVerrouille(){
   if (isset($_SESSION['panier']) && $_SESSION['panier']['verrou'])
   return true;
   else
   return false;
}

/**
 * Compte le nombre d'articles différents dans le panier
 * @return int
 */
function compterArticles()
{
   if (isset($_SESSION['panier']))
   return count($_SESSION['panier']['libelleProduit']);
   else
   return 0;

}

// fonction qui ajoute la commande et le caddie dans les BDD respectives

function validerCommande()
{
   $total=0;
   for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
   {
      $total += $_SESSION['panier']['qteProduit'][$i] * $_SESSION['panier']['prixProduit'][$i];
   }

   $req_ID_CLIENT = 'SELECT ID_CLIENT FROM client WHERE NOM_CLIENT="'.$_SESSION['NOM_CLIENT'].'"';
   $recup_ID_CLIENT = mysql_query($req_ID_CLIENT) or die('Erreur SQL !<br />'.$sql.'<br />'.mysql_error());
   $result_ID_CLIENT = mysql_fetch_row($recup_ID_CLIENT);
   $ID_CLIENT = $result_ID_CLIENT[0];

   // ajout de la commande dans la base commande
   $ajoutcommande = 'INSERT INTO commande (ID_COMMANDE, ID_CLIENT, DATE_COMMANDE, PRIX_TOTAL) VALUES ("","'.$ID_CLIENT.'","'.date('Y-m-d').'" ,"'.$total.'")';
   mysql_query($ajoutcommande) or die('Erreur SQL !'.$ajoutcommande.'<br />'.mysql_error());

   $ID_COMMANDE = mysql_insert_id();

   // ajout des détails de la commande à la base detailscommande

   for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
   {
      $req_ID_BD ='SELECT ID_BD FROM produit WHERE TITRE= "'.$_SESSION['panier']['libelleProduit'][$i].'" ';
      $recup_ID_BD = mysql_query($req_ID_BD) or die('Erreur SQL !<br />'.$req_ID_BD.'<br />'.mysql_error());
      $result_ID_BD = mysql_fetch_row($recup_ID_BD);
      $ID_BD = $result_ID_BD[0];

      $req_REF_BD='SELECT REF_BD FROM produit WHERE TITRE= "'.$_SESSION['panier']['libelleProduit'][$i].'" ';
      $recup_REF_BD = mysql_query($req_REF_BD) or die('Erreur SQL !<br />'.$req_REF_BD.'<br />'.mysql_error());
      $result_REF_BD = mysql_fetch_row($recup_REF_BD);
      $REF_BD = $result_REF_BD[0];

      $ajout_details_commande = 'INSERT INTO detailscommande (ID_COMMANDE, QUANTITE, PRIX, TITREBD, ID_BD, REF_BD) VALUES ("'.$ID_COMMANDE.'","'.$_SESSION['panier']['qteProduit'][$i].'","'.$_SESSION['panier']['prixProduit'][$i].'","'.$_SESSION['panier']['libelleProduit'][$i].'","'.$ID_BD.'", "'.$REF_BD.'")';
       mysql_query($ajout_details_commande) or die('Erreur SQL !<br />'.$ajout_details_commande.'<br />'.mysql_error());
   }



   $message_confirmation= "Votre commande a bien été enregistrée !";
}

// Fonction pour sotcker le caddie pour plus tard

function validerCaddie()
{
   $total=0;
   for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
   {
      $total += $_SESSION['panier']['qteProduit'][$i] * $_SESSION['panier']['prixProduit'][$i];
   }

   $req_ID_CLIENT = 'SELECT ID_CLIENT FROM client WHERE NOM_CLIENT="'.$_SESSION['NOM_CLIENT'].'"';
   $recup_ID_CLIENT = mysql_query($req_ID_CLIENT) or die('Erreur SQL !<br />'.$sql.'<br />'.mysql_error());
   $result_ID_CLIENT = mysql_fetch_row($recup_ID_CLIENT);
   $ID_CLIENT = $result_ID_CLIENT[0];

   // ajout du caddie dans la base caddie

   $ajoutcaddie = 'INSERT INTO caddie (ID_CADDIE, ID_CLIENT, DATE_CADDIE, PRIX_TOTAL_CADDIE) VALUES ("","'.$ID_CLIENT.'","'.date('Y-m-d').'" ,"'.$total.'")';
   mysql_query($ajoutcaddie) or die('Erreur SQL !'.$ajoutcaddie.'<br />'.mysql_error());

   $ID_CADDIE=mysql_insert_id();

   // ajout des détails du caddie à la base details_caddie

   for($i = 0; $i < count($_SESSION['panier']['libelleProduit']); $i++)
   {

      $req_ID_BD ='SELECT ID_BD FROM produit WHERE TITRE= "'.$_SESSION['panier']['libelleProduit'][$i].'" ';
      $recup_ID_BD = mysql_query($req_ID_BD) or die('Erreur SQL !<br />'.$req_ID_BD.'<br />'.mysql_error());
      $result_ID_BD = mysql_fetch_row($recup_ID_BD);
      $ID_BD = $result_ID_BD[0];      

      $ajout_detail_panier='INSERT INTO details_caddie (ID_DETAILS, ID_CADDIE, QUANTITE_BD, PRIX_UNITAIRE, TITRE_BD, ID_BD) VALUES ("","'.$ID_CADDIE.'","'.$_SESSION['panier']['qteProduit'][$i].'" ,"'.$_SESSION['panier']['prixProduit'][$i].'","'.$_SESSION['panier']['libelleProduit'][$i].'","'.$ID_BD.'")';
      mysql_query($ajout_detail_panier) or die('Erreur SQL !<br />'.$ajout_detail_panier.'<br />'.mysql_error());
   }

   $message_confirmation= "Votre caddie a bien été sauvegardé !";
}

// fonction pour modifier un produit déjà existant 

function modifierProduit(){
   extract($_GET);
   $reqSql="UPDATE `produit` SET REF_BD='$_POST[refBD]', HEROS='$_POST[herosBD]', TITRE='$_POST[titreBD]', PRIX_PUBLIC='$_POST[prixpublicBD]', ID_FOURNISSEUR='$_POST[idfournisseurBD]', PRIX_EDITEUR='$_POST[prixediteurBD]',`ID_BD`='$_POST[id_bd]', ID_AUTEUR='$_POST[idauteurBD]', ID_GENRE='$_POST[idgenreBD]', RESUME='$_POST[resumeBD]', REF_FOURNISSEUR='$_POST[reffournisseurBD]', REF_EDITEUR='$_POST[refediteurBD]' WHERE `produit`.`ID_BD`='$_POST[id_bd]'";
   mysql_query($reqSql) or die('Erreur SQL !<br />'.$reqSql.'<br />'.mysql_error());
}

// fonction pour modifier une commande déjà existante

function modifierCommande($id_commande){
   extract($_POST);
}

function supprimerCommande(){
   extract($_POST);
   $reqSql="DELETE FROM commande WHERE ID_COMMANDE=$id_commande";
   mysql_query($reqSql) or die('Erreur SQL !<br />'.$reqSql.'<br />'.mysql_error());
   $reqSql2="DELETE FROM detailscommande WHERE ID_COMMANDE=$id_commande";
   mysql_query($reqSql2) or die('Erreur SQL !<br />'.$reqSql2.'<br/>'.mysql_error());
   $message_confirmation = "La commande à bien été effacée de la base de données";
}

function updateInfos(){
   extract($_POST);
   $reqSql="UPDATE client SET COURRIEL='$COURRIEL',NOM_CLIENT='$NOM_CLIENT', 
      PRENOM_CLIENT='$PRENOM_CLIENT', ADRESSE='$ADRESSE', TEL='$TEL', MOT_DE_PASSE='$MOT_DE_PASSE', 
         ADRESSE_LIVRAISON='$ADRESSE_LIVRAISON', NOM_LIVRAISON='$NOM_LIVRAISON', PRENOM_LIVRAISON='$PRENOM_LIVRAISON' WHERE NOM_CLIENT='$NOM_CLIENT' ";
   mysql_query($reqSql) or die('Erreur SQL !<br />'.$reqSql.'<br />'.mysql_error());
}
?>