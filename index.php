<?php
session_start();
include 'config.inc.php';
include_once "fonctions.inc.php"; 

   if(isset($_GET['titre'])) ajouterArticle($_GET['titre'],$_GET['quantite'],$_GET['prix']);
   if(isset($_POST['viderpanier'])) supprimePanier();
   if(isset($_POST['validercommande'])) validercommande();
   if(isset($_POST['validerCaddie'])) validerCaddie();
   if(isset($_POST['modifProduit'])) modifierProduit();
   if(isset($_POST['modifCommande'])) modifierCommande();
   if(isset($_POST['supprimerCommande'])) supprimerCommande();
   if(isset($_POST['update_infos'])) updateInfos();

   if(isset($_GET['page'])) $_SESSION['page']=($_GET['page']);
?>

<?php include "header.php" ?>
<?php include "site.php" ?>
<?php include "footer.php" ?>